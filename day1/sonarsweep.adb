with ADA.TEXT_IO; use ADA.TEXT_IO;

procedure SonarSweep is
	type MeasurementWindowRange is range 1..3;
	procedure IncreaseIndex(Index : IN OUT MeasurementWindowRange) is
	begin
		if (Index < MeasurementWindowRange'Last) then
			Index := Index + 1;
		else
			Index := MeasurementWindowRange'First;
		end if;
	end IncreaseIndex;
	type MeasurementArrayType is Array(MeasurementWindowRange) of Integer;
	function MeasurementArraySum(MeasurementArray : IN MeasurementArrayType) return Integer is
		Sum : Integer;
	begin
		Sum := 0;
		for Index in MeasurementWindowRange loop
			Sum := Sum + MeasurementArray(Index);
		end loop;
		return Sum;
	end MeasurementArraySum;

	Line : String(1..10);
	LineLength : Integer;
	File : File_Type;
	MeasurementArrayIndex : MeasurementWindowRange;
	MeasurementArray : MeasurementArrayType;
	CurrentDepthSum, LastDepthSum : Integer;
	IncreaseNumber : Integer;
begin
	IncreaseNumber := 0;
	Open(File, IN_FILE, "input.txt");
	for Index in MeasurementWindowRange loop
		GET_LINE(File, Line, LineLength);
		MeasurementArray(Index) := Integer'Value(Line(Line'First..LineLength));
		MeasurementArrayIndex := Index;
	end loop;
	LastDepthSum := MeasurementArraySum(MeasurementArray);

	while not END_OF_FILE(File) loop
		GET_LINE(File, Line, LineLength);
		MeasurementArray(MeasurementArrayIndex) := Integer'Value(Line(Line'First..LineLength));
		IncreaseIndex(MeasurementArrayIndex);
		CurrentDepthSum := MeasurementArraySum(MeasurementArray);
		if (CurrentDepthSum > LastDepthSum) then
			IncreaseNumber := IncreaseNumber + 1;
		end if;
		LastDepthSum := CurrentDepthSum;
	end loop;
	Close(File);
	Put_Line("Number of increases : " & IncreaseNumber'Image);
end SonarSweep;
