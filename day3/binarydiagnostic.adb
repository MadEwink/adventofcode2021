with ADA.TEXT_IO; use ADA.TEXT_IO;
with ADA.Containers.Vectors;

procedure BinaryDiagnostic is
	subtype MyString is String(1..16);
	File : File_Type;
	Line : MyString;
	Line_Length : Natural;
	package LineVectorType is new ADA.Containers.Vectors(Natural, MyString);
	use LineVectorType;
	AllBitsVector : LineVectorType.Vector;
begin
	Open(File, IN_FILE, "input.txt");
	Get_Line(File, Line, Line_Length);
	declare
		Bits_Length : constant Natural := Line_Length;
		type Bits_Range is new Integer range 1..Bits_Length;
		Bits_Number : Array(Bits_Range) of Natural;
		Total : Natural;
		Epsilon_Rate : Natural;
		function Gamma_Rate(Epsilon_Rate : Natural) return Natural is
			Complement : Natural;
			type Unsigned_Integer is mod 2**16;
		begin
			Complement := (2**Bits_Length) - 1;
			return Integer(Unsigned_Integer(Complement) xor Unsigned_Integer(Epsilon_Rate));
		end Gamma_Rate;
	begin
		for Index in Bits_Range loop
			Bits_Number(Index) := 0;
		end loop;
		Total := 0;

		loop
			LineVectorType.Append(AllBitsVector, Line);
			Total := Total + 1;
			for Index in Bits_Range loop
				if Line(Integer(Index)) = '1' then
					Bits_Number(Index) := Bits_Number(Index) + 1;
				end if;
			end loop;

			exit when End_Of_File(File);
			
			Get_Line(File, Line, Line_Length);
		end loop;

		Close(File);

		Epsilon_Rate := 0;
		for Index in Bits_Range loop
			if Bits_Number(Index) >= (Total / 2) then
				Epsilon_Rate := Epsilon_Rate + 2**(Bits_Length - Integer(Index));
			end if;
		end loop;

		Put_Line("Epsilon Rate : " & Epsilon_Rate'Image & ", Gamma Rate : " & Natural'Image(Gamma_Rate(Epsilon_Rate)));
		Put_Line("Power consumption : " & Natural'Image(Epsilon_Rate * Gamma_Rate(Epsilon_Rate)));

		declare
			type ArrayRange is new Integer range 0..Total-1;
			type EliminatedType is Array(ArrayRange) of Boolean;
			OxygenEliminated : EliminatedType;
			CO2Eliminated : EliminatedType;
			NumberOxygenEliminated : Natural := 0;
			NumberCO2Eliminated : Natural := 0;
			MostCommonBit : Boolean;
			LeastCommonBit : Boolean;
			OxygenRating : Natural;
			CO2Rating : Natural;
			function GetFromCriteria(BitIndex : Bits_Range; Eliminated : EliminatedType; CriteriaIsMost : Boolean) return Boolean is
				TotalLines : Natural := 0;
				TotalOnes : Natural := 0;
			begin
				for LineIndex in ArrayRange loop
					if (not Eliminated(LineIndex)) then
						TotalLines := TotalLines + 1;
						if AllBitsVector.Element(Integer(LineIndex))(Integer(BitIndex)) = '1' then
							TotalOnes := TotalOnes + 1;
						end if;
					end if;
				end loop;
				if CriteriaIsMost then
					return (TotalOnes >= TotalLines-TotalOnes);
				else
					return (TotalOnes < TotalLines-TotalOnes);
				end if;
			end GetFromCriteria;
			function ToInt(Line : MyString) return Natural is
				Result : Natural := 0;
			begin
				for Index in 1..Bits_Length loop
					if Line(Index) = '1' then
						Result := Result + 2**(Bits_Length-Index);
					end if;
				end loop;
				return Result;
			end ToInt;
		begin
			for LineIndex in ArrayRange loop
				OxygenEliminated(LineIndex) := false;
				CO2Eliminated(LineIndex) := false;
			end loop;
			SearchBits:
			for BitIndex in Bits_Range loop
				MostCommonBit := GetFromCriteria(BitIndex, OxygenEliminated, True);
				LeastCommonBit := GetFromCriteria(BitIndex, CO2Eliminated, False);
				for LineIndex in ArrayRange loop
					if (NumberOxygenEliminated < Total-1 and (not OxygenEliminated(LineIndex))) and ( ( MostCommonBit and AllBitsVector.Element(Integer(LineIndex))(Integer(BitIndex)) = '0' ) or ( (not MostCommonBit) and AllBitsVector.Element(Integer(LineIndex))(Integer(BitIndex)) = '1' ) ) then
						OxygenEliminated(LineIndex) := true;
						NumberOxygenEliminated := NumberOxygenEliminated + 1;
					end if;
					if (NumberCO2Eliminated < Total-1 and (not CO2Eliminated(LineIndex))) and ( ( LeastCommonBit and AllBitsVector.Element(Integer(LineIndex))(Integer(BitIndex)) = '0' ) or ( (not LeastCommonBit) and AllBitsVector.Element(Integer(LineIndex))(Integer(BitIndex)) = '1' ) ) then
						CO2Eliminated(LineIndex) := true;
						NumberCO2Eliminated := NumberCO2Eliminated + 1;
					end if;
					exit SearchBits when NumberOxygenEliminated = Total-1 and NumberCO2Eliminated = Total-1;
				end loop;
			end loop SearchBits;

			for LineIndex in ArrayRange loop
				if not OxygenEliminated(LineIndex) then
					OxygenRating := ToInt(AllBitsVector.Element(Integer(LineIndex)));
					Put_Line("Oxygen generator rating : " & AllBitsVector.Element(Integer(LineIndex))(1..Bits_Length) & " = " & OxygenRating'Image);
				end if;
				if not CO2Eliminated(LineIndex) then
					CO2Rating := ToInt(AllBitsVector.Element(Integer(LineIndex)));
					Put_Line("CO2 scrubber rating : " & AllBitsVector.Element(Integer(LineIndex))(1..Bits_Length) & " = " & CO2Rating'Image);
				end if;
			end loop;

			Put_Line("Life support rating : " & Natural'Image(OxygenRating*CO2Rating));
		end;
	end;
end BinaryDiagnostic;
