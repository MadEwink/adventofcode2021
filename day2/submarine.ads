with ADA.TEXT_IO; use ADA.TEXT_IO;

package Submarine is
	type CoordinatesType is
		record
			Horizontal : Integer;
			Depth : Integer;
		end record;
	type SubmarineType is
		record
			Coordinates : CoordinatesType;
			Aim : Integer;
		end record;
	type DirectionType is (Forward, Down, Up);
	type CommandType is
		record
			Direction : DirectionType;
			Value : Natural;
		end record;
	InputError : exception;
	procedure ReadCommand(File : IN File_Type; Command : OUT CommandType);
	procedure ProcessCommand(Submarine : IN OUT SubmarineType; Command : IN CommandType);
end Submarine;
