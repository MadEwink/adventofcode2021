with ADA.TEXT_IO; use ADA.TEXT_IO;
with Submarine;

procedure Main is
	SubmarineVessel : Submarine.SubmarineType;
	Command : Submarine.CommandType;
	File : File_Type;
begin
	Open(File, IN_FILE, "input.txt");
	SubmarineVessel.Coordinates.Horizontal := 0;
	SubmarineVessel.Coordinates.Depth := 0;
	while not END_OF_FILE(File) loop
		Submarine.ReadCommand(File, Command);
		Submarine.ProcessCommand(SubmarineVessel, Command);
	end loop;

	Put_Line("Horizontal : " & SubmarineVessel.Coordinates.Horizontal'Image & ", Depth : " & SubmarineVessel.Coordinates.Depth'Image);
	Put_Line("Multiplied Value : " & Integer'Image(SubmarineVessel.Coordinates.Horizontal*SubmarineVessel.Coordinates.Depth));
end Main;

